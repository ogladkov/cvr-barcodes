import sys
sys.path.append('yolov5')
import os
from doctest import debug
import cv2

from fastapi import FastAPI, UploadFile, File
from fastapi.responses import FileResponse
import uvicorn
from yolo_fast import YOLOFast


app = FastAPI()
yolofast = YOLOFast()

@app.post("/run")
def upload(img: UploadFile = File(...)):
    img_fname = img.filename.split(".")[0]
    init_img_location = f'./images/input/{img_fname}_init.jpg'  # initial image location
    with open(init_img_location, 'wb') as img_file:
        img_file.write(img.file.read()) # save image

    # YOLO
    bboxes = yolofast.run(init_img_location)
    img = cv2.imread(init_img_location)
    for bbox in bboxes:
        img = cv2.rectangle(img, tuple(bbox[:2]), tuple(bbox[2:]), color=(0,255,0), thickness=2)
    bbimg_fname = init_img_location.replace('.jpg', '_bbox.jpg')
    cv2.imwrite(bbimg_fname, img)

    return FileResponse(bbimg_fname)


if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=8001, reload=False, log_level="debug", debug=True,
                workers=os.cpu_count(),
                # limit_concurrency=1,
                # limit_max_requests=1,
                timeout_keep_alive=0)